#!python
import os, subprocess

def sys_exec(args):
    proc = subprocess.Popen(args, stdout=subprocess.PIPE)
    (out, err) = proc.communicate()
    return out.rstrip("\r\n").lstrip()

platform = ARGUMENTS.get("platform", ARGUMENTS.get("p", "osx"))
target = ARGUMENTS.get("target", "debug")
godot_headers_path = ARGUMENTS.get("headers", os.getenv("GODOT_HEADERS", "/Volumes/Slave/Downloads/godot-3.0-dev/modules/gdnative/include/"))
godot_bindings_path = ARGUMENTS.get("cpp_bindings", "cpp_bindings/")
dynamic = ARGUMENTS.get("dynamic", "yes")

# This makes sure to keep the session environment variables on windows, 
# that way you can run scons in a vs 2017 prompt and it will find all the required tools
env = Environment()
if platform == "windows":
    env = Environment(ENV = os.environ)
elif platform == "ios":
    SDK_MIN_VERSION = "8.0"
    # we could do better to automatically find the right sdk version
    SDK_VERSION = "11.2"
    IOS_PLATFORM_SDK = sys_exec(["xcode-select", "-p"]) + "/Platforms"
    env["CXX"] = sys_exec(["xcrun", "-sdk", "iphoneos", "-find", "clang++"])

if ARGUMENTS.get("use_llvm", "no") == "yes" and platform != "ios":
    env["CXX"] = "clang++"

# put stuff that is the same for all first, saves duplication
cpp_bindings_libname = 'libgodot_cpp_bindings_%s.a' % platform
if platform == "osx":
    env.Append(CCFLAGS = ['-g','-O3', '-std=c++14', '-arch', 'x86_64'])
    env.Append(LINKFLAGS = ['-arch', 'x86_64', '-framework', 'Cocoa', '-Wl,-undefined,dynamic_lookup'])
elif platform == "ios":
    env.Append(CCFLAGS = ['-g','-O3', '-std=c++11', '-arch', 'arm64', '-arch', 'armv7', '-arch', 'armv7s', '-isysroot', '%s/iPhoneOS.platform/Developer/SDKs/iPhoneOS%s.sdk' % (IOS_PLATFORM_SDK, SDK_VERSION) , '-miphoneos-version-min=%s' % SDK_MIN_VERSION])
    env.Append(LINKFLAGS = ['-arch', 'arm64', '-arch', 'armv7', '-arch', 'armv7s', '-isysroot', '%s/iPhoneOS.platform/Developer/SDKs/iPhoneOS%s.sdk' % (IOS_PLATFORM_SDK, SDK_VERSION) , '-miphoneos-version-min=%s' % SDK_MIN_VERSION])
elif platform == "linux":
    env.Append(CCFLAGS = ['-g','-O3', '-std=c++14', '-Wno-writable-strings'])
    env.Append(LINKFLAGS = ['-Wl,-R,\'$$ORIGIN\''])
elif platform == "windows":
    if target == "debug":
        env.Append(CCFLAGS = ['-EHsc', '-D_DEBUG', '/MDd'])
    else:
        env.Append(CCFLAGS = ['-O2', '-EHsc', '-DNDEBUG', '/MD'])
    env.Append(LINKFLAGS = ['/WX'])
    cpp_bindings_libname = 'godot_cpp_bindings'

def add_sources(sources, dir):
    for f in os.listdir(dir):
        if f.endswith(".cpp"):
            sources.append(dir + "/" + f)

env.Append(CPPPATH=[godot_headers_path, godot_bindings_path + 'include/', godot_bindings_path + 'include/core/' ])

env.Append(LIBS=[cpp_bindings_libname])
env.Append(LIBPATH=[ godot_bindings_path + 'bin/' ])

sources = []
add_sources(sources, "src")

def change_id(self, arg, env):
    sys_exec(["install_name_tool", "-id", "@rpath/libsimple_library_%s.dylib" % platform, "bin/libsimple_library_%s.dylib" % platform])

# determine to link as shared or static library
if dynamic == "yes":
    library = env.SharedLibrary(target='bin/simple_library_%s' % platform, source=sources)
else:
    library = env.StaticLibrary(target="bin/simple_library_%s" % platform, source=sources)

# can't figure it out what type of parameter should be at 1st one
# send in '' and it works
if dynamic == "yes":
    change_id_action = Action('', change_id)
    AddPostAction(library, change_id_action)
    Default(library)
